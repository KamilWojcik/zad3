Kamil Wójcik
Ulubione miejsce na Ziemi
07.11.2021
Puławy
---
## Ulubione miejsce na Ziemi
**Paryż** jest stolicą Francji. Miasto położone jest nad Sekwaną. Stanowi centrum
polityczne, ekonomiczne i kulturalne kraju. Miasto zamieszkuje ok. 17 200 000 osób. Aglomeracja paryska jest najwiekszą w Unii Europejskiej 

## Miejsca do zwiedzania
Miejsca, które warto odwiedzić w Paryżu

1. Wieża Eiffla
2. Luwr
3. Katedra Notre Dame
4. Łuk Triumfalny
5. Pola Elizejskie

## Wieża Eiffla
![](wieza.jpg)

## Dane do prezentacji
[wikipedia](https://pl.wikipedia.org/wiki/Pary%C5%BC)

## Obrazek

![](paryz.jpg){ height=50% width=50%}

## Obrazek wycentrowany z podpisem

![&nbsp; Paryż](paryz1.jpg){ height=50% width=50%}

## Tabela

| Odwiedzający miasto rocznie       | Pieniądze wydane przez turystów     | Pieniądze wydane przez mieszkańców
| ------------- |:-------------:| -----:|
| 50 milionów    | 16 miliardów | 3 miliardy |


